<!-- 2/1/2019 -->

# Advertising 
  ## Twitter
    - Find Individuals          - ❓️
    - Find Advertising Networks - ❓️

# Revenue -> Infoproduct

  ## Serverless
    - Netlify   : ✔️
    - AWS Lamda : ❓️

  ## Handling Money
    - Stripe : ✔️
  <!-- Something that you could do is upon a stripe webhook (charge.success I think), send the email to the user. You will need their email anyway for invoices. -->

  ## Delivery System
    - https://sendgrid.com/ ✔️    
    - Amazon S3 ❓


<!-- 2/2/2019 -->

# Follow up plans 1+ year

  ## Police Record Concerning the Incident
   ## Private Investigator / Attorney  
    - Commissioned Outreach Services 
      -> Who was contacted regarding me (motel neuroplastition person)
      -> List of paranoia inducing 'entertainment' that was commissioned
      -> Full medical records collected by the police at Fairview prior to Lutheran transfer  

  ## Fairview Police
    - Setup a social media service dedicated to explaining the actions regarding my Incident
    - Create website section explaining the link between tactics used and between law enforcement as a whole. Specifically that while it is rational to assume that law enforcement around the country utilize these tactics, it's likely that their participation as departments around the country individually is less complete. 
      -> Condoning Child abuse
      -> Abuse of Surveillance
      -> Misunderstanding of basic human rights
      -> Abuse of constitutional rights
      -> Attempts at Personal Life Sabotage
      -> Abusing Dr Patient Confidentiality
      -> Attempts to discover and create new psychological torture methods through 24/7 observation with illegal surveillance
      -> Manipulation of online Accounts (reddit)
      -> Sabotage of Organization using false authority claiming I was a drug dealer