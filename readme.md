

# Todo

- Clean up Images and connecting text that relate to the images in each section

<!-- 1/29/2019 -->
## Behavior Reinforcement
  - Needs image touchup
  - Missing Connecting Text, better explanation of the way that Law Enf use operant conditioning to help reinforce controllable behaviors during times of great stress in order to instigate response that they will be able to take advantage of.
    
    When individuals are in a panicked state or have made sense of a stimulus after longterm exposure and there is a consistent response from the individual, that stimulus can be manipulated by law enforcement in private environments that the individuals assisting law enforcement may be unaware of. 
      ie: Law enforcement may be putting the devices that create stimulus that triggers the Person of Interest into their homes without the owners knowing that they are multipurposed devices.

    <!-- Personal Case: 1-beep = yes 2-beep = no 3/4-beep = Question -->
    Every individual may receive the patterns and stimulus differently. The core idea is for law enforcement to have a means of control where they are able to influence their actions remotely. This limits the liability they face(somewhat), and also keeps them out of obvious sight of the individual they are trying to control. Ideal circumstances are always that the individual that they are trying to control is never aware that they are present.

    These neutral stimuli that law enforcement use to control individuals can be used in various circumstances. These are especially effective if the individual is close to a psychotic break, which isn't an uncommon response to the type and severity of the stress they are exposed to in these types of situations.

    By allowing a pattern to develop with extended exposure to the stimulus, law enforcement create an avenue of control that is part of the core cycle of
    ->inducing a panic state by engineering circumstances
    ->observe response and use to induce more psychological stress/torture
    ->instigating even better controllable behavior.

    -

## Pillars of Control
  - Missing gfx for Isolation and Instigating Illegal Behavior
  - Missing Connecting Text

  <!-- 1/30/2019 -->
## Link Between Tactics used, Terrorism, and The Constitutional Crisis this Presents

  - Section discussing how these tactics are similar to tactics used by terrorists. 
  - The nature of these tactics are highly unconstitutional.

  - Focuses on Destabilization, Fear, Inescapability, and loss of control of circumstances. Goes even further to try and individually orchestrate and engineer chaos in a person's life. This is why it's one of the core focuses of these tactics to ensure that the target never understands who is doing this to them.
  
  - In some ways they are worse than the tactics used at Guantanmo Bay because that focused primarily on the physical aspects of sensory torture. These tactics are of the same level and/or worse due to relationship damage that a person can endure as it relates to their life.

  - Explain some of the dangers of these tactics being utilized over an extended period of time and how this will cause an increase of domestic terrorism. 
  
  - Explain the problems of a federally backed institution, the police, using these tactics in any meaningful way and how that not only erodes their place in society as individuals responsible for enforcing laws, as the nature of these tactics involves numerous occassions of breaking the law, but also undermines the purpose of citizen's constitutional rights.

<!-- 1/31/2019 -->
## Good Additional Expansion Sections

  - Dealing with the illegal listening devices
    - locations = vents, ceiling, signs of differing texture
    - Pay attention to where you are when noises are syncing up with your actions. Observe your surroundings and think about where a camera would need to be for them to have a visual of you.

    - in neutral settings (apartments / Motels / Hotels)
    - in family/friends/acquaintances settings (trust manipulation, police will lie to f&f to get them involved)
      - Understanding how law enforcement try to play both sides of every emotional interaction with family/friends
  
  - Understanding the placing of noise device -> Any open vents ducts / spaces in closet or above blinds. Basically anywhere they can cover up.

  - Digital Devices being compromised.
    - Car Systems, phones, computer, television(gifts from friends / acquaintences), radio

  - Everyday Nuisances to increase frustrations
    - Primary concepts behind these things rely upon the person whose being targeted to be too panicked and stressed to deal with these problems in a rational way / and are inline with what the standard goal of the law enf cycle of stress. Instigate Controllable Behavior  
    - Door Locks
    - Slow drains (sand / gravel)

<!-- 2/1/2019 -->

  - Psychotic Breaks and Inducing Paranoia Goals + Syncing with noises => beyond operant conditioning
    - A level of control by instigating behavior that law enforcement can take advantage of
    - Enforcing self doubt as much as possible, the more self doubt that the target has, the more room there is for law enforcement to influence the individual with additional suggestions. 
      - By inducing this level of self-doubt, law enforcement can also try to make suggestions towards potential insecurities as well. These can come from videos that the target is watching, songs that have lyrics that may be relevant to law enforcements goal of suggesting certain mindsets 
    - If the target becomes aware of law enforcements involvement, law enforcement will try and induce self-doubt in the target's guilt or innocence in relation to the situation or in general. It doesn't matter if there is anything for the target to feel guilt over. It's just a matter of inducing the state of panic.
    
    - 1. engineer circumstances (implied guilt/distressful emotions synced with noises) 
    - 2. observe response with illegal surveillance
    - 3. try to use to induce more implied guilt with more circumstances if possible / helps if paranoia is also being induced or target is panicking 
    - 4. Attempt to enflame response as much as possible in order to instigate better controllable behavior

    - If the target begins exhibiting behavior that law enforcement doesn't want to occur, previously observed combinations of stimuli will be used to try and induce previously observed responses. Law enforcement really do not want to lose control. The best way to counter these things is literally to understand the circumstances and ignore them. There are always reprecussions for their actions, they want the target to believe that there are none, and that the target is helpless to the circumstances.

<!-- 1/31/2019 -->
## Shaking Loose Whats there Implications

  - The primary idea of the way law enf operate in these situations is that they use anything that the person already has beliefs for in some way in order to create some kind of destabilization in their life. If the person is racist or scared of an ethnic group, that group will be used to harass them. If that person is insecure about their sexual identity, there will be more people around them to talk about them looking gay.

  - Literally anything that they can use to make a person feel uncomfortable / insecure can and will be used to control / destabilize the individual
    - Child abuse

  - If the person figures out what is happening, law enf will do their best to try and treat it like it was a joke, one of the easiest ways to see through these types of operations is by understanding that law enf want to take the easiest path of manipulation to ensure complete control.
  
  - If the target has family and friends that they trust, they will do their best to make sure that they are on the side of law enf. They will never tell them about the true motives of their intent and they will obfuscate the truth the entire time.
    - Law enf will try to ask cursory questions about what the target might be afraid of / make them uncomfortable, likely with the guised interest of 'we don't want to make them feel uncomfortable, is there anything we shouldn't do'
  
   - Additionally, law enforcement tries to use as many 3rd parties as possible whenever possible in order to manipulate targets. This allows them a layer of insulation during these operations. 
    
  - Online Manipulations of accounts is common. Law enforcement are not unwilling to log into your account to change things in order to induce paranoia or cover their tracks (twitter / reddit)

<!-- 2/1/2019 -->

## Basis for About me Section

  - The police were unsure if I was a drug dealer or not based on me having a prescription for adderall and some of the things I had said while they had me panicking when I fled the original house to the police station, they then began using these tactics on me which for a while worked.
  
  - I had a breakdown after being at the motel for 4 weeks and they assumed, that I was either hiding that I was a drug dealer or that they could get away with making me forget about the entire thing and assume it was just a lot of very 'strange' people and a bad place where I was living. 

  - Basically the logic that they used in my situation to control people in my family and those in my life was that they believed that I might be a drug dealer, or that I was a drug dealer. The truth of the situation was that they wanted to have the outcome they could control regardless of the truth. They only covered their bases with what they could and hoped that the final outcome with me would be something they could utilize.

  <!-- 2/2/2019 -->

  - It would make sense to assume this way of trying to manipulate my situation with regards to telling everyone in my life that I was a drug dealer would be similar to the way they would manipulate other people who are 'assisting' them with their setup operations.

  - For example, during my time living at the house off of craigslist my other primary roomate was also assisting with erratic behavior and he had been living there for a roughly a year prior to me being there. This leads me to believe that they try to use the same line of reasoning of 'you're with us, you can help us break the law and you'll be fine' similar to the way that they tried to use that line of reasoning with me when i moved into my apartment. They wanted me to commit a crime so that they would have some kind of leverage on me. At the same time, they didn't want me to actually get some kind of momentum against them so whenever I tried to explain my situation, they would tell people that I was a drug dealer.
    - Organization members
    - Doctor
    - Aunt
    - Mother
