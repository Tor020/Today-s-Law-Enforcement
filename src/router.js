import Vue from 'vue'
import Router from 'vue-router';
import Landing from './views/Landing.vue';
import WhiteBoardNote from './views/WhiteBoardNote.vue';
import OperantConditioning from './views/OperantConditioning.vue'
import Generic from './views/Generic.vue';
import Control from './views/Control.vue';
import SelfIdentify from './views/SelfIdentify.vue';
import PillarsOfControl from './views/PillarsOfControl.vue';
import FAQ from './views/FAQ.vue';




Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing',
      component: Landing
    },
    {
      path: '/FAQ',
      name: 'FAQ',
      component: FAQ
    },

    {
      path: '/whiteBoardNote',
      name: 'whiteBoardNote',
      component: WhiteBoardNote
    },

    {
      path: '/control',
      name: 'control',
      component: Control
    },
    {
      path: '/operantConditioning',
      name: 'operantConditioning',
      component: OperantConditioning
    },
    {
      path: '/selfIdentify',
      name: 'selfIdentify',
      component: SelfIdentify
    },
    {
      path: '/pillarsOfControl',
      name: 'pillarsOfControl',
      component: PillarsOfControl
    },
    {
      path: '/generic',
      name: 'generic',
      component: Generic
    }
  ]
})
